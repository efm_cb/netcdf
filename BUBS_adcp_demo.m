%% FUnction to produce the final matrix of data contained in Greg's 2017 Le JGR submission
clear all; close all
hdir='./TestData';

%% Everything in the Meta structure will be appended globally to Netcdf file
% Look "write_netcdf_global.m" for the various types of metadata that can
% be passed.

%Author metadata, useful to have
Meta.author_email='cynthia.bluteau@gmail.com';
Meta.author= 'Cynthia Bluteau';
Meta.institution='UWA';

% Meta about the general field experiment
Meta.fieldExperiment='BUBS-NRL adapter';
Meta.cruiseDate='April 2012';
Meta.site_nominal_depth=105;
Meta.geospatial_lat=-19.6933;
Meta.geospatial_lon=116.11;


%% Specific to instrument being stored
Meta.featureType='mooredTimeSeries'; % VERY important to specify. Can also provide cell array of dimension variable names
% Meta.featureType={'TIME','HEIGHT_AB'};
Meta.abstract='Processed ADCP velocities from the 300 kHz ADCP sampling over the extent of the  BUBS mooring (~40 m) during the NRL field experiment on the Australian NWS. ';
Meta.instrument= '300 kHz ADCP RDI Teledyne';


%% Do ADCPs
cd(hdir)
load('ADCP_BUBS','adcp')
    
vf={'heading','pitch','roll','east_vel','north_vel','up_vel','error_vel',...
    'totalwater_depth','pressure','depth','z','mtime','corr','intens'};

% This simply strips off all excessive variables stored in adcp structure. Also maps the names of exisintg variables to those recognized ISO/IMOS standards. 
% The idea is that the fields must match the short_names within the
% ImosParameters.txt file. I generated a new one for turbulence purposes.

Dat=clean_fieldnames(adcp,vf); 
Dat=strip_time(Dat,[datenum(2012,4,2) datenum(2012,4,25)],'TIME'); % optional
                    
Meta.instrument_sample_interval=[num2str(round(mode(diff(adcp.mtime))*24*3600)),' s'];


 
create_netcdf_fielddata(Dat,Meta,'ADCP_BUBS',0);% 0=flat netcdf file will be written
