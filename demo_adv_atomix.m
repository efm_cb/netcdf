% Example demo to read and write ATOMIX adv data.
% Load the entire toolbox onto your path
%clear all; close all
hdir='./TestData'; % Relative path to where the data is located. Could forgo if path loaded.

%% Load field data and global metadata
%FieldData=load(fullfile(hdir,'TTIDE_6329_noP_1p4m'),'Level1_raw','Level2_qaqc_velocities','Level2_segmented');

FieldData=load(fullfile(hdir,'TTIDE_6019_0p4m'),'Level1_raw','Level2_qaqc_velocities','Level2_segmented');
timeRef=datenum(2000,1,1,0,0,0); % to adjust time variable unit of reference. The code presumes you supplied time in matlab datenum format 

% FieldData: structure where each field will be written to its own NetCDF
% group. Each group can haave its own dimensions defined via field
% featureTyoe.
% Meta:structure with global metadata. All fields will be written into the
%   NetCDF, except for Meta.featureType/Meta.dimensions (default dimension) 
%% Read global and group level metadata via yaml input files
cd(hdir)

Meta = ReadYaml('tshelf_global_metadata.yml',[],1);
GroupMeta = ReadYaml('adv_atomix_metada.yml',[],1);
%% Load optional flags that explain meangings of flags in variables_QC
% You can edit the yml file to suit your definitions. This YAML can also
% contain extra variable attributes that aren't in the CSV databases.
% Anything in this custom yaml will override other databases...
ADVFlags = convert_flags_yaml('adv_qc_flags.yml');

% Add flags to FieldData
vF=fieldnames(ADVFlags);
gF=fieldnames(FieldData);
mF=fieldnames(GroupMeta);
nGrp=length(gF); % nbre Groups
for ii=1:nGrp
    if any(strcmp(gF{ii},vF))
        FieldData.(gF{ii}).Flags=ADVFlags.(gF{ii});
    end
    
    if any(strcmp(gF{ii},mF))
        FieldData.(gF{ii}).Meta=GroupMeta.(gF{ii});
    end
end



%% Write the file

create_netcdf_fielddata(FieldData,Meta,'ADV_atomix_test',1,timeRef);