function create_netcdf_fielddata(FieldData,Meta,oname,tgrp,timeRef)
% create_netcdf_fielddata(FieldData,Meta,oname,tgrp,timeRef)
%   Accomodates Structures in FieldData where each field is a separate
%   group! e.g., FieldData.summer and FieldData.winter for summer and
%   winter cruises, or these could be different instruments? The name of
%   the structure fields dictate the name of the groups!
% Required inputs:
%	oname: desired  filename for output NetCDF
%	FieldData: struct that I typically save in matlab .mat that I want to convert to netcdf.
%		e.g. FieldData may contain fields with data: PRES,  TIME, TEMP if flat NetCDF
%		or fields that are structures i.e., groups (Level1_raw,
%		Level2_quality_controlled)
%   tgrp=0 or 1 number of levels if the FieldData should be sorted in groups according to
%       each field. tgrp=0 means the NetCDF is "flat", while tgrp=1 means
%       we have one hierarchy level. For ATOMIX data, use tgrp=1.
%
%	Meta: structure with global attributes/metadata that will be writtn to file.
%           This could be nominal depth of water, nominal height of instrument abbove seabed, time deployed and recovered.
%
%   Special fields that should be present at global or group level:
%       Meta.featureType OR Meta.dimensions: dictates the global dimensions via a cell array with names of
%           dimension variables e.g.,Meta.featureType={'TIME','HEIGHT_AB'}
%           featureType can also be a string  (e.g., timeSeries) that will be queried by get_coordinate_type.m to determine the expected dimensions.
%           See get_coordinate_type for different definitions.
%      The global featureType (or dimensions) can be overiden at any group level (e.g.,
%               FieldData.group1) if it contains a field called "featureType".
%       Meta.Flags (purely optional): follows similar principle as Meta.featureType.
%
%   Any group in FieldData with field "Flags" (FieldData.group1.Flags) will overide global Meta.Flags
%           The Flags is structure with fields named "variable_FLAGS" which
%           will append flag attribute information (flag_meanings/flag_values/flag_masks) to the variable
%           FieldData.group1.variable_FLAGS
%           The Flags' fields include only attributes to write into the
%           NetCDF if the variable is found in any of the groups
%
%           For example: "FieldData.group1.UCUR_FLAGS" will have flags that are explained in
%           Meta.Flags.UCUR_FLAGS, which can be overriden by flags explained in
%           FieldData.group1.Flags.UCUR_FLAGS.
%
% Optional input:
%   timeRef: datenum(1950,1,1,0,0,0) is the default time ref applied to
%   ALL. If timeRef is 0, then no adjustments is made (useful when TIME vars are already
%   in julian days)
%   variables that contain the word TIME (TIME_HPR, TIME_BNDS).

%% Data checks and initialisations
if isfield(Meta,'Flags')
    DataFlags=Meta.Flags;
    Meta=rmfield(Meta,'Flags');
else
    DataFlags=struct;
end

if isempty(tgrp)
    tgrp=0;
end

if nargin< 5
    timeRef=datenum(1950,1,1,0,0,0);
end
[featureType,Meta]=check_dimension_feature(Meta);
%% Write global attributes
[~,oname]=fileparts(oname);
fid = netcdf.create(strcat(oname,'.nc'),'netcdf4');
write_netcdf_global(fid,Meta); % Write the global metadata (field experiment, author, etc) to the netcdf file

%% Create/write groups

vF=fieldnames(FieldData);
nGrp=length(vF);

% Grab global group if it exists..
jj= find(strcmpi(vF,'Global'));
if ~isempty(jj)
    Glob=FieldData.(vF{jj});
    if isfield(Glob,'Meta')
        Glob=rmfield(Glob,'Meta'); % glob
    end
else
    Glob=struct;
end



switch tgrp
    case{0} % flat NetCDF
        writegroups(FieldData,fid,featureType,DataFlags,timeRef);
    case{1}
        for ii=1:nGrp
            disp(['Group: ', vF{ii}])
            childId(ii) = netcdf.defGrp(fid,vF{ii}); % define group
            
            dataType=check_featuretype(FieldData.(vF{ii}),featureType); % grab groups metadatatype
            FieldData.(vF{ii})=add_group_metadata(FieldData.(vF{ii}),childId(ii));
            
            if isfield(FieldData.(vF{ii}),{'Flags'})
                Flags=FieldData.(vF{ii}).Flags;
                FieldData.(vF{ii})=rmfield(FieldData.(vF{ii}),'Flags');
            else
                Flags=DataFlags;
            end
            
            if strcmpi(vF{ii},'Global')==1
                writegroups(FieldData.(vF{ii}),fid,dataType,Flags,timeRef);
            else
                writegroups(FieldData.(vF{ii}),childId(ii),dataType,Flags,timeRef,Glob);
            end
        end
end


% close
netcdf.close(fid);

close all

end % end of Main

%% Sub fcts
function [dims,newCell]=writegroups(StnDat,childId,featureType,DataFlags,timeRef,Glob)
if nargin==6
    StnDat=append_struc(StnDat,Glob);% needed if dimensions are stored/written "global" group...
    % there's a glitch though since they will get written anyways :( This
    % code only works for storing global VARIABLES, but not global dim
end


[StnDat,varTim]=adjust_time_for_reference(StnDat,timeRef);
cellData=convert_struc_cell_netcdf(StnDat,DataFlags);
cellData=correct_time_units_attributes(cellData,varTim,StnDat,timeRef);
[dims,newCell]=identify_dimension(cellData,featureType);

if nargin==6
    %dims=remove_global_cells(dims,Glob);
    newCell=remove_global_cells(newCell,Glob);
end

write_netcdf_var(childId,newCell,dims); 
end

%% fct to remove redundant global attributes
function dims=remove_global_cells(dims,Glob)
% Remove cells/dims that are already written via glob variable
vF=fieldnames(Glob);
cc=0;
ind=[];
for ii=1:length(dims)
    if ~strcmp(dims{ii}.short_name,vF)
        cc=cc+1;
        ind(cc)=ii;
    end
end

if ~isempty(ind)
    dims={dims{ind}};
end
end

%% Fct to correct time attributes units as per timeRef
%950-01-01T00:00:00Z
function cellData=correct_time_units_attributes(cellData,varTim,StnDat,timeRef)
vF=fieldnames(StnDat);

for ii=1:length(varTim)
    ind=find(strcmp(varTim{ii},vF));
    if ~isempty(ind)
        cellData{ind}.Atts.units=['Days since ',datestr(timeRef,'yyyy-mm-ddTHH:MM:SSZ')];
    end
end


end

%%
function NewField=add_group_metadata(FieldData,childId)
% assumes group ID has been called/defined before calling this subfct
% e.g., childId = netcdf.defGrp(fid,vF{ii});
%   Looks for structure "Meta", and assigns all fields to netcdf as
%   metadata at the group level.
%Output: NewField which is the same as FieldData but with Meta structure removed.
aF={'Meta','META'}; % so

for ii=1:length(aF)
    if isfield(FieldData,aF{ii})
        
        [~,TmpMeta]=check_dimension_feature(FieldData.(aF{ii}));
        
        
        mF=fieldnames(TmpMeta);
        groupVarID = netcdf.getConstant('GLOBAL'); % as in the current group
        
        for kk=1:length(mF)
            netcdf.putAtt(childId,groupVarID, mF{kk},TmpMeta.(mF{kk})); % or moored
        end
        FieldData=rmfield(FieldData,aF{ii});
    end
end

NewField=FieldData;
end

%%

function dataType=check_featuretype(Data,defaultType)
% Checks for Meta field in each group, so that different feature types may
% Input: Data Structure for the group
%       Meta: global metadata structure to set default

aF={'Meta','META'}; % sometimes tools will uppercase all variable names
dataType=defaultType; % default type is set globally

for ii=1:length(aF)
    if isfield(Data,aF{ii})
        [dataType,~,tdef]=check_dimension_feature(Data.(aF{ii}));
        if tdef==1
            dataType=defaultType;
        end
        % if strcmp(Data.(aF{ii}),'featureType')
        %    dataType=Data.(aF{ii}).featureType;
        %end
    end
end

end
%%
%
% function [FieldData,childId]=addnew_group(FieldData,fid,tgrp,lev)
% % lev is the depth we're processing
% vF=fieldnames(FieldData);
% nGrp=length(vF);
%
% for ii=1:nGrp
%     disp(['Group: ', vF{ii}])
%     childId(ii) = netcdf.defGrp(fid,vF{ii}); % define group
%     if tgrp>0 % global one have already been placecd
%         FieldData.(vF{ii})=add_group_metadata(FieldData.(vF{ii}),childId(ii));
%     end
%     %if lev==tgrp
%     writegroups(FieldData.(vF{ii}),childId(ii),'timeSeries',DataFlags)
%     %end
% end
% end

function [featureType,Meta,tdef]=check_dimension_feature(Meta)
% FUnction checks if a featureType or dimension was specified
% Input:
%   Meta: structure of attributes... the returned structure will habe
%   dimensions variable removed...
vF={'dimensions','dimensionType','featureType'}; % for backward compatibility
tDim=isfield(Meta,vF);
tdef=0;
if any(tDim)
    ii=find(tDim);
    if length(ii)==1
        featureType=Meta.(vF{ii});
        Meta=rmfield(Meta,vF{ii});
    else
        error('Please specify the dimensions field within Meta structure. e.g. Meta.dimensions={"Time","Depth"}')
    end
else
    tdef=1; % using defaults
    warning('Specify your  dimensions within Meta structure. Will attempt using global defaults or  TIME')
    %warning('Supply a feature type, see get_coordinate_type.m (e.g., timeSeries)')
    featureType={'TIME'};%'timeseries';
end


end