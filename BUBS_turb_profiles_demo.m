%% FUnction to produce NetCDF file for turbulence profiles collected at one station for 24 h.
%  It demo how to create 2x types of NetCDF file. 
%   The first has all profiles stored as a continous timeseries of data, while the second
%  groups the data for each profile in the NetCDF file. The 2nd option is
%  more useful when storing CTD/VMP data collected at different stations. I
%  do not usually store profiles individually but will store a
%  profile_number to identify them at ech station.
clear all; close all
hdir='./TestData';

%% Everything in the Meta structure will be appended globally to Netcdf file
% Look "write_netcdf_global.m" for the various types of metadata that can
% be passed.

%Author metadata, useful to have
Meta.author_email='cynthia.bluteau@gmail.com';
Meta.author= 'Cynthia Bluteau';
Meta.institution='UWA';


%% BUBS meta
% Meta for all
Meta.fieldExperiment='BUBS-NRL adapter';
Meta.cruiseDate='April 2012';
Meta.site_nominal_depth=105;
Meta.geospatial_lat=-19.6933;
Meta.geospatial_lon=116.11;
%% load VMP data
cd(hdir)

VMP=load('VMP_BUBS');
profile_name=VMP.profile_name;
VMP=rmfield(VMP,'profile_name');
FieldData=clean_fieldnames(VMP,[]);


% Meta
Meta.featureType={'TIME'}; % is the only dimension here since all profiles have different pressure/depth
Meta.abstract='Processed VMP profiles collected at the BUBS mooring during the NRL field experiment on the Australian NWS. ';
Meta.instrument= 'Microstructure profiler, VMP500 from Rockland Scientific';
Meta.instrument_serial_number= '';
Meta.instrument_sample_interval='512 Hz';




%% Demo for creating groups, one for each profile. 
% First lets organise the data into their indivitual profiles
% Data will contain fields representing each station's data. 
disp('Now creating grouped data NC file');
fname=unique(profile_name);
vF=fieldnames(FieldData);
for ii=1:length(fname)
ind=find(strcmp(profile_name,fname{ii}));
stnF=['profile_',num2str(ii)];
FieldData.profile(ind)=ii; % changing it to a numeric value.
for jj=1:length(vF)
GroupedData.(stnF).(vF{jj})=FieldData.(vF{jj})(ind);
end
end
%   We could also add a field "Meta" i.e, another structure with various
%   global attributes to write for each group (i.e., profile). For example,
%   this is probably where one would store the logged lat/lon for each stn
GroupedData.profile_1.Meta.abstract='First profile at BUBS station';
create_netcdf_fielddata(GroupedData,Meta,'BUBS_VMP_grouped_by_profile',1); 

% Saves it as one flat NetCDF file
create_netcdf_fielddata(FieldData,Meta,'BUBS_VMP_all',0); 