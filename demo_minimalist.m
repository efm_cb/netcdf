% Example minimalist demo of writing a NetCDF w/o any Yaml files. i.e. how
% CBluteau did it in 2017 when she had to write the original toolbox
hdir='./TestData'; % Relative path to where the data is located. Could forgo if path loaded.

%% Load field data and global metadata
FieldData=load(fullfile(hdir,'TTIDE_6019_0p4m'),'Level1_raw','Level2_qaqc_velocities');
timeRef=datenum(2000,1,1,0,0,0); % to adjust time variable unit of reference. The code presumes you supplied time in matlab datenum format 

% FieldData: structure where each field will be written to its own NetCDF
% group. Each group can haave its own dimensions defined via field
% featureTyoe.
% Meta:structure with global metadata. All fields will be written into the
%   NetCDF, except for Meta.featureType/Meta.dimensions (default dimension) 
%% Read global and group level metadata via yaml input files
cd(hdir)

Meta.author='CBluteau';
Meta.dimensions={'TIME','N_VEL_COMPONENT','TIME_HPR'}; % Supply all dimensions that appear throughout the groupsi.e. Level1_raw and Level2_qaqc_velocities
%Meta = ReadYaml('tshelf_global_metadata.yml',[],1);

%% Write the file

create_netcdf_fielddata(FieldData,Meta,'ATOMIX_minalist',1,timeRef);