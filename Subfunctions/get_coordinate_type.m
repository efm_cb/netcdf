function [ coordinates ] = get_coordinate_type( featureType )
%function [ coordinates ] = get_coordinate_type( featureType )
%   Determines the coordinates that should be included for a given
%   featureType
% Improvements
%       This was OK when I had a few types, now I think it would be best to
%       simply provide the coordinates with the data. It would make the
%       tool very general
featureType=lower(featureType);
switch featureType
	case{'mooredtimeseries'}
		coordinates={ 'TIME',  'HEIGHT_AB'};
        
   
        
    case{'bursttimeseries'} % data in a matrix such that each burst is its own row.
        coordinates={ 'TIME',  'N_SAMPLES'};
        
  
    
    case{'verticalProfile'}
		coordinates={'TIME'}; % needs to be fixed, but I rarely bin data onto a regular grid for archiving. 
	case{'timeseries'}
		coordinates={'TIME'};
        
    % ADCP Formats   
     case{'adcptimeseries'} % level 1/2 and 4 data in adcp coordinate system.. 3D to place all beams into same file
        coordinates={ 'TIME',  'R_DIST','BEAM_NUMBER'}; % reaplced HEIGHT_AS with R_DIST
    
    case{'adcp_sectioned_timeseries'} % Level 2a section each burst/section is stored separatetely
        % 3D matrix with height, time, and n_samples
        coordinates={ 'TIME',  'R_DIST','N_SAMPLES','BEAM_NUMBER'};
        
    case{'adcp_structure_function'} % level 3 atomix
        % 3D matrix with height, time, and r_del (deltaR) 
        coordinates={ 'TIME',  'R_DIST','R_DEL','BEAM_NUMBER'};
      
%     case{'adcp_dissipation_level4'} % level 4 atomix, where we start storing data for each beam into a matrix
%          coordinates={ 'TIME',  'R_DIST','BEAM_NUMBER'};
         
    % ADV   
    case{'frequency','spectra'} 
        coordinates={'TIME','FREQ'};
    case{'adv_matrix_spectra'} % Assumes all spectra are stored together as 1 matrix
        coordinates={'TIME','FREQ','BEAM_NUMBER'};
    case{'adv_timeseries'}
        coordinates={'TIME','BEAM_NUMBER'};
    case{'adv_segmented_timeseries'}
    
        coordinates={ 'TIME',  'N_SAMPLES','BEAM_NUMBER'};
        
    otherwise    
        coordinates={'TIME'};
        warning(['Unknown featureType ',featureType,' assuming TIME coordinates']);
end

end

