function [dims,nL]=add_dim(fid,dims)
% dims is a cell array where each cell represents a coordinate "variable" (time, depth, etc) that needs defining
%	dims{1}.name= name of the dimension to define a did
%	dims{1}.data= data to fill to identify length of dimension
%	dims{1}.Atts= struct of attributes to add
%	Outputs additional fields to each cell dims:
%		dims{1}.vid variable id
%		dims{1}.did: dimension id
	for ii=1:length(dims)
	 %[ matlabType ] = netcdf3ToMatlabType( dims{ii}.Atts.netCDFtype);
        
        data=dims{ii}.data;
		nL(ii)=length(data);
		dimAtts=dims{ii}.Atts; % structure of attributes for the given dimension
		nameVar=dims{ii}.short_name;
        disp(nameVar)
		% define dimension
		
		dims{ii}.did = netcdf.defDim(fid, nameVar,nL(ii));
		
		  % define variable associated with dimensions (e.g., time or pressure)
		dims{ii}.vid = netcdf.defVar(fid, nameVar, dims{ii}.netCDFtype, dims{ii}.did);
        
		put_ceb_atts(fid, dims{ii}.vid, dimAtts,nameVar,data);   % adds data and attributes tp variable
		
	end 
		
end