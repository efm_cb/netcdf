function write_netcdf_var(fid,data_to_write,dims)
% write_atts(fname,data_to_write)
% writes data within each cell array data_to_write to a NETCDF file with "fid" as file identifier
% each cell array (e.g., Sample_Data=data_to_write{1}) is a structure  containing the following fields:
%	.standard_name='time or velocity'
%	.data: vector/matrix of data to add to the netcdf
% 	Strongly recommended:
%		.long_name='longer definition of standard_name'
%		.units= 'measurement units e.g., oC or days since 1900-01-01 00:00:00'
% 		.anyotherATT : will be used to assign any other  attribute (metdata) stored in the given file "anyotherATT"
% Dim


[dims,nL]=add_dim(fid,dims);

%[dataType,m,n]=findallstr(data_to_write,nL);

for ii=1:length(data_to_write)
    SampleDat=data_to_write{ii};
    nameVar=SampleDat.short_name;
%          if strfind(nameVar,'BEAM_VEL_FLAGS')
%              disp('stop')
%          end
    disp(nameVar);
    Atts=SampleDat.Atts;
    dataType=SampleDat.netCDFtype;
    
    if isfield(Atts,'dimensions')
        % add function for tranposing data only
        [data,dids]=transpose_user_defined_dim(SampleDat.data,dims,Atts);
        Atts=rmfield(Atts,'dimensions');
    else
        [data,dids,dims]=find_dim_transpose_data(SampleDat.data,dims,fid);
    end
    
    if iscell(data) && strcmp(dataType,'char') % Handling strings/char in a very unelegant way. No guranatees.
        data=char(data);
        [a,b] = size(data); % a= same as time dimension
        %idC = netcdf.defDim(fid,'charlen',b);
        [Tmp,idC]=update_constant_dim(fid,'char',['char ',nameVar],b);
        vid = netcdf.defVar(fid,nameVar,dataType,[dids idC]);
    else
        vid = netcdf.defVar(fid,nameVar,dataType,dids); % getting variable ID
    end
    put_ceb_atts(fid, vid, Atts,nameVar,data);   % adds data and attributes tp variable
end
end % end of main


%%
function [data,dids,dims]=find_dim_transpose_data(data,dims,fid)
% Identify the size of data, and which "dimensions" should be associated with it (time and depth for example)
% If none are defined for the length of data (i.e., 1 value), then a new
% dimension will be defined and stored in dims.

% Identify dimension of the data (varDim=0 (constant), varDim=1 is 1D vector, etc_
% Also tranpose/permutes such that its dimensions are in the same order as
% dims.
[data,varDim]=permute_transpose(data,dims);
mSize=size(data);

nDim=length(dims); % nbre of dimension variables (time/depth etc that exist).
for ii=1:nDim
    nL(ii)=length(dims{ii}.data); % n samples in each dimension
end

% Identify which of the dims matches up

if nDim>=varDim % OK let's identify if we have time /depth etc and transpose
    switch varDim
        case{0} % constant
            ind=find(nL==1);
            if isempty(ind)
                if any([ischar(data) iscell(data)])
                    dType='char';
                    [Tmp,dids]=update_constant_dim(fid,dType,'constant',1);
                    dims{nDim+1}=Tmp;
                else
                    dType='double';
                    dids=[];
                end
                
            else
                dids=dims{ind}.did;
            end
        otherwise
            %case{1,2} % vector/matrix
            cc=0;
            for jj=1:length(mSize)
                ind=find(nL==mSize(jj));
                
                if ~isempty(ind)
                    for tt=1:length(ind)
                        cc=cc+1;
                        dids(cc)=dims{ind(tt)}.did;
                    end
                end
            end
            
    end
    
else
    error('Insufficient dimension variables specified')
end

end


%%

function [Tmp,dids]=update_constant_dim(fid,dataType,long_name,siz)
%tmp.did=dids;
% Adds dimensiions for strings and constants...
% siz: size of the dimension [m,n] or 1, or m.
Tmp.standard_name=long_name;
Tmp.netCDFtype=dataType;
Tmp.Atts.long_name=long_name;
Tmp.Atts.units='';
dids=netcdf.defDim(fid,long_name,siz);

Tmp.did=dids;

end

%%
function [data,dids]=transpose_user_defined_dim(data,dims,Atts)
% This function will tranpose the data assigned with variable based on the
% USER supplied dimensions via the variable's attributes (must be specified
% in the Flags yaml optional file). This function is only required to
% handle the situation when multiple dimensions have the same size makig it
% impossible to identify the dim via the variable's size
% (e.g., find_dim_transpose_data.m and permute_tranpose.m)
dimName=Atts.dimensions;
for ii=1:length(dims)
    alldimNames{ii}=dims{ii}.short_name;
    alldids(ii)=dims{ii}.did;
    nL(ii)=length(dims{ii}.data);
end

for ii=1:length(dimName)
    ind=find(strcmp(dimName{ii},alldimNames));
    if isempty(ind)
        error(['No dimension variable by the name of ',dimName{ii}])
    end
    dids(ii)=alldids(ind);
    expDataSize(ii)=nL(ind); % expected data size
end

dataSize=size(data);
varDim=length(dataSize);
if varDim~=ii
    error(['Did not supply enough dimensions in your YAML file for this variable: ',Atts.standard_name])
end


tt=find(dataSize==expDataSize); % check if var is already of correct size

if isempty(tt)
    switch varDim
        case{1}
            if dataSize(1)==1
                data=transpose(data);
            end
            %         case{2}
            %             data=transpose(data); % premute works
        otherwise
            
            cc=0;
            for jj=1:varDim %3>
                ind=find(dataSize(jj)==expDataSize);
                if ~isempty(ind)
                    cc=cc+1;
                    perInd(cc)=ind; % permutation index.
                end
            end
            data=permute(data,perInd);
    end
end

end

