function Dat=strip_time(Dat,timlim,vFtim)
% Reducing time extent of dataset for out of water data
% Required:
%   Dat: data structure with fields "tim"
%   timlim=tim limits to exclude as out of water
% Optional:
%   vFtim='tim' or 'datestmp' i.e., name of the time variable
%       if none specified, code uses 'tim'

if nargin<3
    vFtim='tim';
end

ind=find(Dat.(vFtim)>=timlim(1) & Dat.(vFtim)<=timlim(2));
nT=length(Dat.(vFtim));

%%
vf=fieldnames(Dat);
for jj=1:length(vf)
    tmp=Dat.(vf{jj});
     mSize=size(tmp);
    if ~isstruct(tmp) & any(mSize==nT)
       
        dim=length(mSize);  
    
        switch dim
            case{2}
            if mSize(1)==nT
                tmp=tmp(ind,:);
            else
                tmp=tmp(:,ind);
            end
            
            case{3}
                kk=find(mSize==nT);
                switch kk
                    case{1}
                    tmp=tmp(ind,:,:);
                    case{2}
                    tmp=tmp(:,ind,:);
                    case{3}
                    tmp=tmp(:,:,ind);
                        
                end
                
        end
        
    end
    Dat.(vf{jj})=tmp;
end
    