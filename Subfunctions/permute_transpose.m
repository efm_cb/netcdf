function [data,varDim]=permute_transpose(data,dims)
% function [data]=permute_transpose_tests(data,dims)
%   Tranpose the data such that the order of its dimensions matches those
%   in the cell array "dims" (same order, which is set by
%   get_coordinate_type.m)
%   Subfunction called by write_netcdf_var.m
% Created by CBluteau when I needed to generalise to multi-dim matrix
% Revisited by Brian Scannel in Feb 2021, who fixed bugs when large number
% of dimensions exist, and a variable (2D) contains the higher order
% dimensions

%% BDS attempt
% %{
% determine lengths of dimension variables is order specified by dims
dimsL = NaN(size(dims));
for ii = 1:numel(dimsL)
    dimsL(ii)=length(dims{ii}.data); % number of values for each dimension
end; clear ii
% check for and ignore single value dimensions
if any(dimsL == 1)
    %warning('=== WARNING === dimension defined with length 1 is unresolved')
    %dimsL(dimsL == 1) = NaN; % ignore any actual dimension with length 1
    varDim=1;
    return;
end
% check for dimensions with the same length
if numel(unique(dimsL)) ~= numel(dimsL)
    error('+++ ERROR +++ dimensions could not be resolved as lengths are not unique')
end 
% add unity option as trailing dimension to support processing of vectors
dimsL(end+1) = 1;

% determine number of data array dimensions
mSize = size(data); % always has length >= 2
% process all options other than constant (for which mSize is [1, 1])
if any(mSize ~= 1) %
    % identify index for each dimension of array by comparing lengths
    dimInd = NaN(size(mSize));
    for ii = 1:numel(dimInd)
        ix = find(dimsL == mSize(ii), 1);
        if isempty(ix)
            error('+++ ERROR +++ no matching dimension for array size')
        else
            dimInd(ii) = ix;
        end
        clear ix
    end; clear ii
    % identify sort index to give required dimensions order
    [~, perInd] = sort(dimInd);
    % apply permutation
    data = permute(data, perInd);
end

% determine effective number of dimensions
varDim = sum(mSize ~= 1);
%}

end