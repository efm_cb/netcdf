function [value] = atomixParameters( short_name, field )
%IMOSPARAMETERS Returns IMOS/ATOMIX compliant standard name, units of measurement,
% data code, fill value, valid min/max value or type given the short parameter name.
%
%
% Inputs:
%   short_name  the parameter name
%   field      - either 'standard_name', 'units', 'positive', 'reference_datum', 'data_code',
%                'fill_value', 'valid_min', 'valid_max' or 'netCDFtype',
%                'long_name'
%
% Outputs:
%   value      - the  standard name, unit of measurement, direction positive, reference datum, data code,
%                fill value, valid min/max value or type, whichever was requested.
%
%
% The list of all IMOS parameters is stored in a file 'imosParameters.txt'
% which is in the same directory as this m-file.
%
% The file imosParameters.txt contains a list of all parameters for which an
% IMOS compliant identifier (the short_name) exists. This function looks up the
% given short_name and returns the corresponding standard name, long name,
% units of measurement, data code, fill value, valid min/max value or type. If the
% given short_name is not in the list of IMOS parameters, a default value is
% returned.
%
% Currently, requests for long name and standard name return the same value,
% unless a long_name was provided in atomixParameters.txt or
% customParameters.txt (yet to be created)
%
%
%
% Modified extensively by CBLUTEAU from 2017-2021 for my own "turbulent" purposes
% Code now looks for different templates of NetCDF conventions with precedence given
% in this order
%       {'customParameters.txt','atomixParameters.txt','cebTurbParameters.txt','imosParameters.txt'};
%
% CBluteau also removed the ridiculous naming of variables by placing them into a dynamic structure,
%  No  need for having two sets of variable name that ever so slightly
%   differ when the look up table are all stored in structure "NewTable"
%
% Copyright (c) 2016, Australian Ocean Data Network (AODN) and Integrated
% Marine Observing System (IMOS).
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
%     * Redistributions of source code must retain the above copyright notice,
%       this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the AODN/IMOS nor the names of its contributors
%       may be used to endorse or promote products derived from this software
%       without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%

narginchk(2, 2);
if ~ischar(short_name), error('short_name must be a string'); end
if ~ischar(field),      error('field must be a string');      end

value = NaN;

% account for numbered parameters (if the dataset
% contains more than one variable of the same name)
match = regexp(short_name, '_\d$');
if ~isempty(match), short_name(match:end) = ''; end



% fNames is the list of possible text files with NetCDF conventions, in the order of
% preference. The first file takes precendence over any defined names that
% may re-appear in the other files.
fNames={'customParameters.txt','atomixParameters.txt','cebTurbParameters.txt','imosParameters.txt'};

persistent NewTable;
%NewTable=[];

if isempty(NewTable)
    disp('===============Reloading all convention files==============')
    % Check which files exist om the path
    for kk=1:length(fNames)
        tExist(kk)=exist(fNames{kk});
    end
    ind=find(tExist);
    fNames=fNames(ind); % same as fNames=fNames(ind);
    if tExist(1)
        str=which(fNames{1});
        warning(['Precendence given to customParameters.txt found on your path:',str]);
        disp('If thats not wanted, then remove it from your path');
    end
    
    nFiles=length(fNames); % nbre of files to read
    for kk=1:nFiles
        fid = fopen(fNames{kk}, 'rt');
        switch fNames{kk} % could check the first lone for number of delimiters
            case{'cebTurbParameters.txt','imosParameters.txt'} % no long_name for these vars
                tmpparams = textscan(fid, '%s%d%s%s%s%s%s%s%f%f%q', ...
            'delimiter', ',', 'commentStyle', '%');
            otherwise % read in an extra column
                tmpparams = textscan(fid, '%s%d%s%s%s%s%s%s%f%f%s%q', ...
            'delimiter', ',', 'commentStyle', '%');
        end
        vF{kk}=['file',num2str(kk)];
        AllData.(vF{kk})=merge_data(tmpparams);
        fclose(fid);
    end
    
    NewTable=AllData.(vF{1});
    for kk=2:nFiles
        NewTable=remove_redundant(NewTable,AllData.(vF{kk}));
    end
end

%% Now find the correct info for the requested short_name
iMatchName = strcmpi(short_name, NewTable.short_name);
if any(iMatchName)
%     switch field
%         case 'standard_name'
%             if ~NewTable.cfCompliance(iMatchName)
%                 value = '';
%             else
%                 value = NewTable.standard_name{iMatchName};
%             end
 %       otherwise
            if iscell(NewTable.(field))
                value=NewTable.(field){iMatchName};
                if strcmpi(value, 'percent'), value = '%'; end
            else
                value=NewTable.(field)(iMatchName);
            end
            
%    end
end

% provide default values for unrecognised parameters
if isnan(value)
    switch field
        case 'standard_name',  value = short_name;
        case 'long_name',      value = short_name;
        case 'units',          value = '?';
        case 'positive',       value = '';
        case 'reference_datum',value = '';
        case 'data_code',      value = '';
        case 'fill_value',     value = [];%999999;
        case 'valid_min',      value = [];
        case 'valid_max',      value = [];
        case 'netCDFtype',     value = 'float';
        %case 'description',     value = '';   
    end
else % Now fix the fill_value to match up the datatype
    switch field
        case{'fill_value'}
            dType = NewTable.netCDFtype{iMatchName};
            if ~any(strcmp(dType,{'char','string'}))% convert everything in double ubless declared as text
                value=str2double(value);
            end
    end
end

end % end of main

%% Subfcts
function NewTable=remove_redundant(MainTable,IMOStable)
% It removes any entries in the 2nd IMOSTable, which may
% already be in MainTable.
% Required inputs:
%   Structures with the various fields written in the look up tables.
%   The mapping occurs in merge_data
% All checks are based on the short_name of course

vF=fieldnames(IMOStable);
[cNames,indMain,indImos]= setxor(MainTable.short_name,IMOStable.short_name);

for jj=1:length(vF)
    NewTable.(vF{jj})=[MainTable.(vF{jj}); IMOStable.(vF{jj})(indImos) ];
end

end

%%
function AllTable=merge_data(turb)
% loads the textscan data into a structure AllTable.
% If no  12th column exists (Description/long_name) than the standard_name  is the long_name
vF={'short_name','cfCompliance','standard_name','units','positive','reference_datum',...
    'data_code','fill_value','valid_min','valid_max','netCDFtype','long_name'};
[m,n]=size(turb);

for ii=1:n
    AllTable.(vF{ii})=turb{ii};
    nVars(ii)=length(AllTable.(vF{ii}));
end
% check for load prob i.e., row too short
[nShort]=length(AllTable.short_name);
ind=find(nVars<nShort);
if ~isempty(ind)
    for ii=1:length(ind)
       AllTable.(vF{ind(ii)}){end+1}=[];
    end
end

% Assign std name when long_name not in the table lookup
if ~isfield(AllTable,'long_name')
    AllTable.long_name=AllTable.standard_name;
end
end