function netdcf_any_struct(Bathy,Meta)
% Bathy: has fields that may have dimensions, or variables that can be NetCDF.
Meta.fieldExperiment='Odyssee St-Laurent Inaugural Winter Campaign';
Meta.abstract='Field observations collected aboard the CCGS Amundsen icebreaker during their winter de-icing operations.';
Meta.contributor_name='Cynthia Bluteau ORCID https://orcid.org/0000-0002-4430-0668';
Meta.cruiseDate='Feb 2018';
Meta.ship='CCGS Amundsen';
% [numrow numcol]=size(Bathy.lon);
% ncid = netcdf.create('somefile.nc','NC_WRITE');
% dimidrow = netcdf.defDim(ncid,'rows',numrow);
% dimidcol = netcdf.defDim(ncid,'length',numcol);
% 
% 
% %% Variables
% vF={'lon','lat','elevation'};
% varid = netcdf.defVar(ncid,'lon','NC_DOUBLE',[dimidrow dimidcol]);
% 
% netcdf.endDef(ncid);
% netcdf.putVar(ncid,varid,Bathy.lon);
% %netcdf.putVar(ncid,varid,Bathy.lon);
% netcdf.close(ncid);

%% Try again
oname='testfileB';
fid = netcdf.create(strcat(oname,'.nc'),'netcdf4');
write_netcdf_global(fid,Meta); % Write the global metadata (field experiment, author, etc) to the netcdf file
cellData=convert_struc_cell_netcdf(Bathy);

vF=fieldnames(Bathy);

range.lon=[-180 180];
range.lat=[-90 90];

% Could Improve this code by checking all var have same dimensions
[numrow, numcol]=size(Bathy.(vF{1}));
dimidrow = netcdf.defDim(fid,'rows',numrow);
dimidcol = netcdf.defDim(fid,'length',numcol);

for ii=1:length(vF)

    nameVar=vF{ii};
      data=cellData{ii}.data;
    Atts=cellData{ii}.Atts;
    if ~strcmp(vF{ii},'height')
        Atts.range=range.(vF{ii});
         Atts.long_name=['L',Atts.long_name(2:end)]
        vid = netcdf.defVar(fid, nameVar, 'NC_DOUBLE', [dimidrow dimidcol]);
    else
       Atts.long_name='Elevation';
        Atts.units='m';
        vid = netcdf.defVar(fid, nameVar, 'NC_SHORT', [dimidrow dimidcol]);
    end
    put_ceb_atts(fid,vid,Atts,nameVar,data)
end
netcdf.close(fid)