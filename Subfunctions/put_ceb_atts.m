function put_ceb_atts(fid,vid,Atts,nameVar,data)
% Having this function separate is a little ridiculous. 
% Its called by write_netcdf_var.m


    %defaultFill=use_default_fill_value(data);
if isfield(Atts,'fill_value') 
    if ~isempty(Atts.fill_value) && ~isnan(Atts.fill_value)
        netcdf.defVarFill(fid,vid,false,Atts.fill_value);
    %else
    %netcdf.defVarFill(fid,vid,false,defaultFill);
    end
    Atts=rmfield(Atts,'fill_value');
% else
%     
% %netcdf.defVarFill(fid,vid,false,defaultFill);
%     
end


vF=fieldnames(Atts);
for ii=1:length(vF)
    netcdf.putAtt(fid,vid,vF{ii},Atts.(vF{ii}));
end




% if strcmp(nameVar,'TIME')==1
%     %netcdf.putVar(fid,vid,data-datenum(1950,1,1,0,0,0));
% else
    netcdf.putVar(fid,vid,data);
%end

end % end of main

