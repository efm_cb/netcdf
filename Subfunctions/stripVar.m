function [NewDat,aF]=stripVar(AllCTD,vF)
% Similar to  merge_allprofiles_struct, but retains all the stns
% together... i.e just takes away uncessary variables at each stn (field)
% within the AllCTD.
% Output:
%      aF: actual available variables
stnName=fieldnames(AllCTD);


for ii=1:length(stnName)
    cc=0;
    Dat=AllCTD.(stnName{ii});
    mN=length(Dat.tim); % Assumes timestamp exists.
    for jj=1:length(vF)
        if isfield(Dat,vF{jj})
            cc=cc+1;
            aF{cc}=vF{jj};
            tmp=Dat.(vF{jj});

            if max(size(tmp))==1
                tmp=tmp.*ones([mN 1]);
            else
                [m,n]=size(tmp);
                if n>m
                    tmp=tmp';
                end
            end


            Tmp.(vF{jj})=tmp;
        
            
        end
    end
    Tmp=clean_fieldnames(Tmp,aF);
    NewDat.(stnName{ii})=Tmp;
    
   clear aF
end




end