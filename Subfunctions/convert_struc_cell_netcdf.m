function [cellData]=convert_struc_cell_netcdf(Data,AllFlags)
%function cellData=convert_struc_cell_netcdf(Data)
% Designed to receive a Data structure where each field (numeric data type)
% gets  converted into a form usable for input into write_netcdf_var
% Eventually will create a template for each variable
% Required inputs:
%	Data: struct with any fields, but only those in the templates will have
%	meaningful attributes (See atomixParameters.m)
%		e.g., Data.epsilon= vector of epsilon, Data.tim=vector of datenum, Data.z=20
% Optional input:
%   AllFlags: structure with fields variables_FLAGS to add flag attributes to Data.variables_FLAGS
%       AllFlags.variable_FLAGS contain structures with flag_values (or flag_masks for boolean/bytes) and flag_meanings
%       that will be extra attributes to the variable_FLAGS (understand what
%       the numbers means in the array!)
% Output:
%   cellData: each cell array is a struc for field of the supplied "Data" found in the databases.
%       each struc contains following fields:
%       cellData{1}.standard_name, .data, .Atts= as listed under attName
%       below
%Optional inputs:
% To do:
%   Allow for optional additional attributes to be written stored in Flags
%   that may have nothing to do with qaqc flags. Ge
%%
if nargin<2
    AllFlags=struct;
end
varName=fieldnames(Data); % Data.tim, Data.epsilon

%desAtt={'long_name','uom','type','fill_value','reference_datum','positive'}; % des attributes to look up for each variable
attName={'standard_name','units','netCDFtype','fill_value','reference_datum','positive','long_name'}; % name to call it in netcdf file
desAtt=attName;

nbrAtt=length(desAtt);
cc=0;
for jj=1:length(varName)
    
    qcName=strfind(varName{jj},'_FLAGS'); % do we have a QC variable
    if isempty(qcName) % finds a qc var to make flags
        stdName=varName{jj};
    else
        stdName=varName{jj}(1:qcName-1);
    end
    
    for ii=1:nbrAtt
        Atts.(attName{ii})= atomixParameters( stdName, desAtt{ii});   
    end
    

    disp(['For debugging, trying to write: ', stdName])
    
    
    if isfield(AllFlags,varName{jj})==1 % checks if  we have flags/yaml definitions for this variable
        if strcmp(stdName,varName{jj})  % we have other var that we want to alter the attributes
            [Atts,Tmp]=append_xtra_attributes(AllFlags.(varName{jj}),Atts,Data.(varName{jj}));      
        else  %we have a QAQC flag
            Atts=rmfield(Atts,'fill_value'); % meaningless fill_value since its read for non-QC data records (e.g., ENU_VEL) as oposed to the flags records (e.g., ENU_VEL_FLAGS)
            [Atts,Tmp]=append_flag_attributes(AllFlags.(varName{jj}),Atts,Data.(varName{jj})); % If no fill_values specified in Yaml input file, the code  will set data to the "unknown" flag if present.   
        end
    else % No Flags or extra attributes
        % If no fill value supplied in table, then defaults from netcdf.putatt library will be used.
        [Tmp.data,Tmp.netCDFtype]=checkdata_type(Data.(varName{jj}),Atts.netCDFtype);
    end
    
    if all([~isempty(qcName), any(strcmp(desAtt,'fill_value'))]) % revert qc flags to default fills
        Atts.fill_value=[];
    end
    
    Tmp.short_name=varName{jj};
    Atts=rmfield(Atts,'netCDFtype'); % no need to specify this as an xtra attribute when
    
    Tmp.Atts=Atts;
    cc=cc+1;
    cellData{cc}=Tmp;
    clear Atts
end

end % end of main


%%
function [newData,netCDFtype]=checkdata_type(data,expectedType)
% Check if the expected netCDFtype (expectedType) found in our databases match up with the data
% provided.
% If expectedType are integers, ubytes (boolean flags), or float we will convert
%   the data to match up with the expectedType.
% netCDFType will be equal expectedType. newData is the converted data.
% Otherwise, the netCDFType will be set to the class/type of supplied  data to match the database (i.e., expectedType)
%       The newData = data!
[netCDFtype] = matlab_to_netcdf_type(class(data));
newData=data;
%if ~isempty(Atts.netCDFtype)
switch expectedType
    case{'int'}
        if ~strcmp(netCDFtype,'int')
            newData=int32(data);
            netCDFtype=expectedType; %'int';
            warning('Changing data to integer as specified by atomixParameters.m or expected for the flag')
        end
    case{'ubyte'}
        if ~strcmp(netCDFtype,'ubyte')
            if any(data>255)
                error('Boolean ubytes should be integers numbers less 255')
            end
            newData=uint8(data);
            netCDFtype=expectedType; %'ubyte';
            warning('Changing data to ubyte as specified by atomixParameters.m or expected for boolean flags')
        end
    case{'float'}
        newData=single(data);
        netCDFtype=expectedType;
end
end

%% function check Flags
function [Atts,Tmp]=append_flag_attributes(Flags,Atts,data)
%We assign the Flags attributes, and correct the data and flag_data so they
%match the expected type (ubyte for boolean, int for enumerated)
% The fill_value obtained from the database (e.g., ENU_VEL) is over-written
% with:
%   (a) what's supplied in Flags (usually in Yaml file). Ideal!!!
%   (b) value equivalent to the unknown flag value.
% If neither is available, then the fill_value is left empty (none will
%   be used when writing the data).

flagChk={'flag_values','flag_masks'}; % enumerated and boolean
dataType={'int','ubyte'};
tflags=isfield(Flags,flagChk);
if all(tflags)
    error('Cannot specify both flag_masks (booleans) and flag_values (enumerated) flags')
end

ii=find(tflags);
[Tmp.data,Tmp.netCDFtype]=checkdata_type(data,dataType{ii}); % data format will
[Flags.(flagChk{ii}),~]=checkdata_type(Flags.(flagChk{ii}),dataType{ii}); % convert to expected datatype for that flag type (boolean vs enumerated)

if ~isfield(Flags,'fill_value')
    flag_meanings=strsplit(Flags.flag_meanings);
    ind=find(strcmp(flag_meanings,'unknown'));
    if isempty(ind)
        Flags.fill_value=[];
    else
        Flags.fill_value=Flags.(flagChk{ii})(ind); % fill with unknown flag
    end
end


if ~isfield(Flags,'standard_name')
    Flags.standard_name= 'status_or_quality_flag';
else
    Flags.standard_name=strcat(Atts.standard_name,'_',Flags.standard_name);
end

% if ~isfield(Flags,'long_name')
%    Flags.long_name=[regexprep(Flags.standard_name,'_',' '),' for ' ,regexprep(Atts.standard_name,'_',' ')];
% end


flagAtt=fieldnames(Flags);
for ii=1:length(flagAtt)
    Atts.(flagAtt{ii})=Flags.(flagAtt{ii});
end

end

%% Add extra attributes from yaml file, not a qaqc flag
% function check Flags
function [Atts,Tmp]=append_xtra_attributes(Flags,Atts,data)
%We assign the extra Flags attributes, and correct the data  so they
%match the expected type. It will over-write any original attribute in Atts

[Tmp.data,Tmp.netCDFtype]=checkdata_type(data,Atts.netCDFtype); % data format will

flagAtt=fieldnames(Flags);
for ii=1:length(flagAtt)
    Atts.(flagAtt{ii})=Flags.(flagAtt{ii});
end

end