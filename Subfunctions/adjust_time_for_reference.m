function [Data,varTim]=adjust_time_for_reference(Data,timeRef)
%function adjust_time_for_reference(Data,timeRef)
% Adjust time variables in the Data structure by substracting timeRef. 
% all variables that contain TIME in their shortname will be corrected
% (TIME_BNDS, TIME_CTD, etc). 
% Required inputs:
%   Data: structure with many fields, some with variable TIME or TIME_XX
% 
% Optional
%  timeRef: datenum(1950,1,1,0,0,0) or another reference time. Assumes data
%  in days. 
% output:
%   Data struct with each variable properly corrected
%   varTim: string of variables adjusted...

if isempty(timeRef)
    timeRef=datenum(1950,1,1,0,0,0);
end


aF=fieldnames(Data);
tmp=strfind(aF,'TIME'); % any var starts with word TIME will be adjusted!
cc=0;
varTim=cell(0);
for jj=1:length(aF)
    if tmp{jj}==1 
        Data.(aF{jj})=Data.(aF{jj})-timeRef;
        cc=cc+1;
        varTim{cc}=aF{jj};
    end
end

