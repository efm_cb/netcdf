function VMPclean=clean_fieldnames(ADV,vf)
%function VMPclean=clean_fieldnames(ADV,vf)
% Cleans up many of CEB variables into matrix sizes compliant with
% IMOS/ATOMIX naming
%  Required inputs
%       ADV: structure with various fields of data to save in netcdf/clean
%       vf: cell array of fields to retain.
%          set vf=[] if you want retain all existing fields in struct ADV
%           These will be converted into uppercase.
%
% Bandage function to fix various variable names, so that they match with
% CF-complicant databases located in imosParameters.txt or imosTurbParameters.txt
% Otherwise, the create_netcdf_fielddata won't be able to find the correct
% attributes.
%
% Other users can re-write their own version that accomodates their naming
% convention
%
% It will also separate variables, such as matrices of multi-beam matrixes into single beam matrices/vector correlation matrices, and
%   For example, a 3-beam ADV has a 3 column matrix of correlation when NetCFD file
%   wants these separated into 3 different vectors named corr1, corr2, and
%   corr3
%
% Improvements & limitations
%       This is an adhoc fix for cynthia's messy variable names, so they
%       can be found in the text files loaded by atomixParameters.m

%%
% if nargin<2 % vars to retain.... if present
%     vf={'I','KLe','Kt','Le','Lk','Lo','Lrho','Ls','N','P','Rf','Ri','Rif',...
%         'S','Tz','W','bind','chiF','chiF_DO','chiI','chiT','dTdz','date',...
%         'drhodz','dudz','east_vel','enu','epsilon','error_vel','fname','kd','mN2','mTz',...
%         'meanU','mtime','north_vel','nu','p','pressure','rho','segm','tayl','tim','time',...
%         'up_vel','uvw','wDO_ecm','wDO_idm','xyz','z'}; % S=shear
%
% end

if isempty(vf)
    vf=fieldnames(ADV);
end

%%
VMPclean=struct;
cc=0;
for jj=1:length(vf)
    tSq=0; % no 3D array
    if isfield(ADV,vf{jj})
        tmp=ADV.(vf{jj});
        cc=cc+1;
        
        mSize=size(tmp);
        mL(cc)=max([mSize]);
        if length(mSize)<3
            if mSize(1)<mSize(2)
                tmp=transpose(tmp);
            end
            
        else
            tSq=1;
        end
        
        %% Switch
        switch vf{jj}
            case{'lon','LON'}
                pF='LONGITUDE';
                
            case{'lat','LAT'}
                pF='LATITUDE';
                
            case{'time','tim','date','mtime','datestmp','TIM'}
                pF='TIME';
              
              
            case{'P','p','pressure','pres'}
                pF='PRES';
                
            case{'z','height','Z'}
                pF='HEIGHT_AB';
                
            case{'DepSM','depSM','depth'}
                pF='DEPTH';
                
            case{'nom_depth'}
                pF='NOMINAL_DEPTH';
                
            case{'totalwater_depth'}
                pF='BOT_DEPTH';
                
            case{'N2'}
                tmp=sqrt(tmp);
                pF='BVFSQ';
            case{'N'}
                pF='BVFSQ';
                
            case{'enu'}
                %pF={'EAST_VEL','NORTH_VEL','UP_VEL'};
                pF={'UCUR','VCUR','WCUR'}; % CF compliant version
                
            case{'uvw'}
                pF={'U_VEL','V_VEL','W_VEL'};% rotated into frame of ref
                
            case{'xyz'}
                pF={'X_VEL','Y_VEL','Z_VEL'};
                
            case{'dudz'}
                pF={'dUVEL_dZ','dVVEL_dZ','dWVEL_dZ'};
                
            case{'denudz'} % gradients in geographical coordinates
                pF={'dUCUR_dZ','dVCUR_dZ','dWCUR_dZ'};
                
            case{'SAL','SAL00','Sal00','salinity'}
                pF='PSAL';
                
            case{'T','meanT','T090C','SBT'}
                pF='TEMP';
                
            case{'phosphate'}
                pF='PHO2'; % assumes conc per L of seawater
                
            case{'silicate'}
                pF='SLCA'; % assumes conc per L of seawater
                
            case{'nitrateFlux','flux_N'}
                pF='NTRA_FLUX';
                
                
            case{'proxyNitrate','proxy_nitrate','PROXY_NTRA'}
                pF='PROXY_NTRA';
                
                
            case{'nitrite'}
                pF='NITRITE';
                
            case{'epsilon','epsi'}
                vC={'U','V','W'};
                switch mSize(2)
                    case{3}
                        
                        for zz=1:mSize(2)
                            pF{1,zz}=['EPSI',vC{zz}];
                        end
                    case{1}
                        pF='EPSI'; % could be the average or final value
                        
                    otherwise
                        for zz=1:mSize(2)
                            pF{1,zz}=['EPSI',num2str(zz)];
                        end
                        
                end
                
                
                
            case{'chi','chiT'}
                pF='CHI_TEMP';
                
                
                
            case{'chi_DO','chiF_DO','CHI_DO'}
                pF='CHI_DO';
                
                
            case{'wDO_Flux','wDO','wDO_FLUX'}
                pF='wDO_FLUX';
                
                
            case{'Rf','Rif','Ri_f'}
                pF='Ri_f';
                
                
            case{'Kt','Ktheta'}
                pF='K_TEMP';
                
                
            case{'Ko'} % Osborn's
                pF='K_O';
                
                
            case{'KLe','KL'}
                pF='K_Le'; % mixing estimated from Prandtl mixing via Ellison scale
                
                
                
            case{'S'} % mean Shear
                pF='SH';
                
                
                
            case{'dTdz','Tz'} % mean vertical T gradient
                pF='dTEMP_dZ';
                
                
             % SHIPS
                
                % length scales
            case{'LE','Le','L_e'}
                pF='L_E';
                
                
            case{'L_K','Lk','L_k','eta'}
                pF='L_K';
                
                
                
            case{'Lrho','Lp'}
                pF='L_RHO';
                
                
            case{'L_S','Ls','L_s'}
                pF='L_S';
                
                
                
            case{'L_O','Lo','L_o'}
                pF='L_O';
                
                
                % FLuid properties
            case{'nu','visc'}
                pF='KVISC';
                
                
            case{'kd','kappa_T','kappaT'}
                pF='KAPPA_TEMP';
                
                
            case{'turbMask'}
                pF='MASK';
                
                
            case{'fname','profile_filename'}
                pF='PROFILE_NAME';
                
                
            case{'stnName','station_name'}
                pF='station_name';
                
                
            case{'dropSpeed','drop_speed','DropSp','W'}
                pF='DESC';
                
            case{'meanU'}
                pF='PSPD_REL'; % mean current relative to platform (typically as measured by said platform)
                
            case{'dist_Quebec_city','dist_quebec_city'}
                pF='dist_quebec_city';
                
                
            case{'discharge','flow_rates','flow_rate'}
                pF='discharge';
                
            case{'bind'}
                pF='Mask';
                bind=tmp;
                tmp=zeros([max(mL) 1]);
                tmp(bind)=1;
                
                
            case{'hpr'}
                
                tSq=0; % no squeeze
                pF={'HEADING','PITCH','ROLL'};
                
            case{'corr'} % assumes z*nbre beams * tim matrix
                
                if length(mSize)==3
                    tSq=1; % squeeze
                end
                
                if mSize(2)==3
                    pF={'CORR1','CORR2','CORR3'}; % ADV one
                else
                    pF={'CORR_B1','CORR_B2','CORR_B3','CORR_B4'}; % ADCP one in different uinits
                end
                
            case{'ampli'}
                
                %tSq=0; % no squeeze
                pF={'ABSIC1','ABSIC2','ABSIC3'};
                
                
            case{'snr','SNR'}
                
                % tSq=0; % no squeeze
                pF={'SNR1','SNR2','SNR3'};
                
            case{'intens'} % assumes z*nbre beams * tim matrix
                
                if length(mSize)==3
                    tSq=1; % squeeze
                end
                
                pF={'ABSI1','ABSI2','ABSI3'};
                if mSize(2)==4
                    pF=[pF 'ABSI4']; %{'CORR_b1','CORR_b2','CORR_b3','CORR_b4'};
                end
                
            case{'pctgd'}
                pF='PERGT';
                
                
            case{'z_ASB','mab'}
                pF='HAB';
                
            otherwise
                pF=upper(vf{jj});
                %disp(pF)
                
        end
        
        if ischar(pF)==0
            if tSq==0
                for kk=1:length(pF)
                    VMPclean.(pF{kk})=tmp(:,kk);
                end
            else
                for kk=1:length(pF)
                    VMPclean.(pF{kk})=squeeze(tmp(:,kk,:));
                end
            end
        else
            VMPclean.(pF)=tmp;
        end
        
    end
end