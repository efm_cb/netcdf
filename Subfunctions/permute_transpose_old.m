function [data,varDim]=permute_transpose(data,dims)
% function [data]=permute_transpose_tests(data,dims)
%   Tranposde the data such that the order of its dimensions matches those
%   in the cell array "dims" (same order, which is set by
%   get_coordinate_type.m)
%   Subfunction called by write_netcdf_var.m
% Created by CBluteau when I needed to generalise to multi-dim matrix
mSize=size(data);
% Identify dimension of the data (varDim=0 (constant), varDim=1 is 1D vector, etc_ 
if all(mSize==1)
	varDim=0;
    return;
else
	if any(mSize==1)
		varDim=1; % 1D vector
	else
		varDim=length(mSize); % matrix of dim N (2D or larger)
	end
end

nDim=length(dims); % nbre of dimension variables (time/depth etc that exist).
for ii=1:nDim
    nL(ii)=length(dims{ii}.data); % n samples in each dimension
end


cc=0;
for jj=1:length(mSize)
  ind=find(mSize(jj)==nL);
  if ~isempty(ind)
      cc=cc+1;
      perInd(cc)=ind; % permutation index. Actually dims var associated with each dim of data
  end
end



if cc==1 % 1D vector 
     if mSize(1)==1
          data=transpose(data);
          mSize=size(data);
     end
     return;
else %2D
     if varDim==1
        varDim=2; % odd situation when we have a 2nd dimension 1x value long. So a 2D matrix 
     end
end


%disp('Now transpose 2D/3D so they match the dim')
if varDim==nDim
    data=permute(data,perInd);
    return;
end % we have data that doesn't use all dim (so 2D when 3D dim available)


%% Deal with varDim<nDim
df=[diff(perInd) 1];
ind=find(df<0 & perInd>varDim);
if ~isempty(ind)
    dfDim=perInd(ind)-varDim;
    perInd(ind)=perInd(ind)-dfDim;
else
    %ind=find(df>0); % all ascending perInd
    if all(df>0) % nDim>varDim, order of dim is OK but not right ones    
        %perInd=perInd-min(perInd)+1;
        return;
    end
end


switch varDim
    case{2}
        if perInd(1)>1.01
            data=permute(data,perInd);
        end
    case{3}
    data=permute(data,perInd);
end


end